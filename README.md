# EPYC

The game is publically available at www.epycgame.com
The design files are available at https://www.figma.com/files/project/1499540/EPYC!
I use Firebase Firestore https://console.firebase.google.com/u/2/project/vue-epyc/overview
for the database, functions and storage for the images

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production (Currently required for Deployment)
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run on local area network
```
npm run serve-lan
```

### Deploy on Google Cloud (build first)
```
gcloud app deploy
```

### Check latest Google Cloud deploy
```
gcloud app browse
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
