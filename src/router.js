import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Game from './views/Game.vue'
import Games from './views/Games.vue'
import MyGames from './views/MyGames.vue'
import NewGame from './views/NewGame.vue'
import Register from './views/Register.vue'
import Stats from './views/Stats.vue'
import Updates from './views/Updates.vue'
import CompletedGames from './views/CompletedGames.vue'
import InProgressGames from './views/InProgressGames.vue'
import Scoreboard from './views/Scoreboard.vue'
const fb = require('./firebaseConfig.js');
import VueAnalytics from 'vue-analytics';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      redirect: '/games'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/completed-games',
      name: 'CompletedGames',
      component: CompletedGames
    },
    {
      path: '/updates',
      name: 'Updates',
      component: Updates
    },
    {
      path: '/secret',
      name: 'InProgressGames',
      component: InProgressGames,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/games',
      name: 'Games',
      component: Games,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/game/:id',
      name: 'Game',
      component: Game,
      meta: {
        requiresAuth: false
      }
    },{
      path: '/my-games',
      name: 'MyGames',
      component: MyGames,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/new-game',
      name: 'NewGame',
      component: NewGame,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/scoreboard',
      name: 'Scoreboard',
      component: Scoreboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/stats',
      name: 'Stats',
      component: Stats,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

Vue.use(VueAnalytics, {
  id: 'UA-131471877-1',
  router
});

router.beforeEach((to, from, next) => {
  const currentUser = fb.auth.currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else next();
});

export default router;
