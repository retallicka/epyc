import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';

// firebase init goes here

// var config = {
//   apiKey: "AIzaSyCaQR1CTTcCzWrDEEfst_jN7IWxQFwTh2g",
//   authDomain: "vue-epyc-staging.firebaseapp.com",
//   databaseURL: "https://vue-epyc-staging.firebaseio.com",
//   projectId: "vue-epyc-staging",
//   storageBucket: "vue-epyc-staging.appspot.com",
//   messagingSenderId: "82853272387"
// };



var config = {
  apiKey: "AIzaSyAjaYvhKMGFiW6jD-0jWG03IQp6JLSjPcI",
  authDomain: "vue-epyc.firebaseapp.com",
  databaseURL: "https://vue-epyc.firebaseio.com",
  projectId: "vue-epyc",
  storageBucket: "vue-epyc.appspot.com",
  messagingSenderId: "503344375230"
};
firebase.initializeApp(config);

// firebase utils
const db = firebase.firestore();
const auth = firebase.auth();
const auth_s = firebase.auth;
const currentUser = auth.currentUser;
const functions = firebase.functions;

// date issue fix according to firebase
const settings = {
  timestampsInSnapshots: true
};
db.settings(settings);

// firebase collections
const usersCollection = db.collection('users');
const gamesCollection = db.collection('games');
const likesCollection = db.collection('likes');

export {
  db,
  auth,
  auth_s,
  currentUser,
  functions,
  usersCollection,
  gamesCollection,
  likesCollection
}
