const fb = require('../firebaseConfig.js');

export default {
  methods: {
    signinWithFacebook: function() {
      let vm = this;
      var provider = new fb.auth_s.FacebookAuthProvider();
      provider.addScope('email');
      fb.auth.signInWithPopup(provider).then(result => {
        if (result.additionalUserInfo.isNewUser) {
          vm.user = result.user;
          vm.askForName = 'To register please give a display name for your account';
        } else {
          this.successfulLogin(result.user);
        }
      }, error => {
        console.log(error);
        if (error.code === 'auth/account-exists-with-different-credential') {
          console.log('account exists with different creds');
          vm.existingEmail = error.email;
          vm.pendingCred = error.credential;
          // Lookup existing account’s provider ID.
          return fb.auth.fetchSignInMethodsForEmail(error.email)
          .then(function(providers) {
            if (providers.indexOf(fb.auth_s.EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD) !== -1) {
              // Password account already exists with the same email.
              // Ask user to provide password associated with that account.
              vm.askForPassword = 'To link your Facebook account please provide existing EPYC password for ' + vm.existingEmail;
            }
          })
        } else {
          alert('Oops. ' + error.message);
        }
      });
    },
    linkAccount: function() {
      let vm = this;
      fb.auth.signInWithEmailAndPassword(vm.existingEmail, vm.password).then(result => {
        // Existing email/password or Google user signed in.
        // Link Facebook OAuth credential to existing account.
        return result.user.linkAndRetrieveDataWithCredential(vm.pendingCred);
      }, error => {
        alert('Oh no, ' + error.message);
      })
      .then(result => {
        // successfully linked the account, they now need to be logged in
        this.successfulLogin(result.user);
      }, error => {
        alert('Oops, ' + error.message);
      })
    },
    validateName: function(name) {
      let error;
      if (name.length < 3) {
        error = 'Your name is too short. Try something longer';
      } else if (name.length > 15) {
        error = 'Your name is too long. Try something shorter';
      }
      return error;
    },
    registerNewUser: function(user, name) {
      let vm = this;
      let error;
      error = this.validateName(name);
      if (!error) {
        user.updateProfile({
          displayName: name
        }).then(function () {
          vm.$store.commit('setCurrentUser', user);
          fb.usersCollection.doc(user.uid).set({
            name: name,
            adminUser: false
          }).then(() => {
            vm.$store.dispatch('fetchUserProfile');
            vm.$router.push('games');
          }).catch(err => {
            console.log(err);
          })
        }, (err) => {
          error = err.message;
        });
      }
      if (error) {
        alert('Ooops, ' + error);
      }
    },
    confirmNamedAccount: function() {
      this.registerNewUser(this.user, this.name);
    },
    successfulLogin(user) {
      this.$store.commit('setCurrentUser', user);
      this.$store.dispatch('fetchUserProfile');
      this.$router.push('games');
    }
  }
}