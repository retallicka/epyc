export default {
  methods: {
    getUser(state, userID) {
      let index = state.users.findIndex(x => x.id == userID);
      if (index != -1) {
        return state.users[index].name;
      } else {
        return '';
      }
    }
  }
}