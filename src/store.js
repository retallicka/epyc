import Vue from 'vue';
import Vuex from 'vuex';
import Utils from './mixins/Utils.js'
const fb = require('./firebaseConfig.js');

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    currentUser: null,
    userProfile: {},
    allGames: null,
    games: null,
    inProgressGames: null,
    completedGames: null,
    completedGamesByEdited: null,
    completedGamesByLikes: null,
    completedGamesByName: null,
    myGames: null,
    myGamesInProgress: null,
    users: []
  },
  actions: {
    clearData({ commit }) {
      commit('setCurrentUser', null);
      commit('setUserProfile', null);
      commit('setGames', null);
    },
    fetchUserProfile({ commit, state }) {
      fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
        commit('setUserProfile', res.data())
      }).catch(err => {
        console.log(err)
      })
    },
    signOut() {
      return fb.auth.signOut();
    },
    getCompletedGames({ commit }) {
      // currently used if you are offline and need an update
      fb.gamesCollection.orderBy('editedOn', 'desc').get().then(querySnapshot => {
        let gamesArray = [];

        querySnapshot.forEach(doc => {
          let game = doc.data();
          game.id = doc.id;
          if (game.usersTakenPart.length === 6) {
            gamesArray.push(game);
          }
        });
        commit('setCompletedGames', gamesArray);
      });
    }
  },
  mixins: [Utils],
  mutations: {
    // mutations commit and track state changes - ACTIONS CALL MUTATIONS WHICH UPDATE STATE
    setCurrentUser(state, val) {
      state.currentUser = val;
    },
    setUserProfile(state, val) {
      state.userProfile = val;
    },
    setCompletedGames(state, val) {
      state.completedGames = val;
    },
    setGames(state, val) {
      if (val) {
        state.allGames = val;
        let games = val.filter(game => !game.usersTakenPart.includes(state.currentUser.uid));
        games = games.filter(game => game.usersTakenPart.length < 6);
        state.games = games;


        state.inProgressGames = val.filter(game => game.usersTakenPart.length < 6);
        state.inProgressGames = state.inProgressGames.sort((a, b) => b.editedOn.seconds - a.editedOn.seconds);


        state.completedGames = val.filter(game => game.usersTakenPart.length === 6);
        state.completedGames.reverse();
        state.completedGamesByLikes = state.completedGames.slice().sort((a, b) => b.likes - a.likes);
        state.completedGamesByName = state.completedGames.slice().sort((a, b) => a.createdByName.localeCompare(b.createdByName));
        state.completedGamesByEdited = state.completedGames.slice().sort((a, b) => b.editedOn.seconds - a.editedOn.seconds);


        state.myGames = state.completedGames.filter(game => game.usersTakenPart.includes(state.currentUser.uid));
        state.myGamesInProgress = state.inProgressGames.filter(game => game.usersTakenPart.includes(state.currentUser.uid));
        state.myGamesInProgress = state.myGamesInProgress.sort((a, b) => b.posts.length - a.posts.length);
      } else {
        state.games = null;
      }
    },
    setUsers(state, val) {
      state.users = val;
    }
  }
});


// handle page reload
fb.auth.onAuthStateChanged(user => {
  if (user) {
    store.commit('setCurrentUser', user);

    fb.gamesCollection.orderBy('editedOn').onSnapshot(querySnapshot => {
      let gamesArray = [];

      querySnapshot.forEach(doc => {
        let game = doc.data();
        game.id = doc.id;
        gamesArray.push(game);
      });
      store.commit('setGames', gamesArray);
    });

    fb.usersCollection.orderBy('name').onSnapshot(querySnapshot => {
      let usersArray = [];

      querySnapshot.forEach(doc => {
        let user = doc.data();
        user.id = doc.id;
        usersArray.push(user);
      });
      store.commit('setUsers', usersArray);
      store.dispatch('fetchUserProfile');
    });
  }
});