const functions = require('firebase-functions');
const admin = require('firebase-admin');

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
admin.initializeApp(functions.config().firebase);
const gcs = admin.storage();
const Busboy = require('busboy');
const fs = require('fs');
const os = require('os');
const path = require('path');

const firestore = admin.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);






const nodemailer = require('nodemailer');
// Configure the email transport using the default SMTP transport and a GMail account.
// For Gmail, enable these:
// 1. https://www.google.com/settings/security/lesssecureapps
// 2. https://accounts.google.com/DisplayUnlockCaptcha
// For other types of transports such as Sendgrid see https://nodemailer.com/transports/
// TODO: Configure the `gmail.email` and `gmail.password` Google Cloud environment variables.
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

const APP_NAME = 'EPYC Game';

/**
 * Sends a welcome email to new user.
 */
exports.sendWelcomeEmail = functions.auth.user().onCreate((user) => {
  const email = user.email; // The email of the user.
  const displayName = user.displayName; // The display name of the user.

  return sendWelcomeEmail(email, displayName);
});


/**
 * Send an account deleted email confirmation to users who delete their accounts.
 */

exports.sendByeEmail = functions.auth.user().onDelete((user) => {
  const email = user.email;
  const displayName = user.displayName;

  return sendGoodbyeEmail(email, displayName);
});

// Sends a welcome email to the given user.
async function sendWelcomeEmail(email, displayName) {
  const mailOptions = {
    from: `${APP_NAME} <gameepyc@gmail.com>`,
    to: email,
  };

  // The user subscribed to the newsletter.
  mailOptions.subject = `Welcome to ${APP_NAME}!`;
  mailOptions.text = `Hey ${displayName || ''}! Welcome to ${APP_NAME}. Remember to come back and visit epycgame.com. `;
  mailOptions.text += `The game aims to help people improve their drawing skills and have fun. Check back regularly . `;
  mailOptions.text += `See the completed games at https://www.epycgame.com/completed-games`;
  await mailTransport.sendMail(mailOptions);
  console.log('New welcome email sent to:', email);
  return null;
}

// Sends a goodbye email to the given user.
async function sendGoodbyeEmail(email, displayName) {
  const mailOptions = {
    from: `${APP_NAME} <noreply@firebase.com>`,
    to: email,
  };

  // The user unsubscribed to the newsletter.
  mailOptions.subject = `Bye!`;
  mailOptions.text = `Hey ${displayName || ''}!, We confirm that we have deleted your ${APP_NAME} account.`;
  await mailTransport.sendMail(mailOptions);
  console.log('Account deletion confirmation email sent to:', email);
  return null;
}
exports.editGame = functions.firestore.document('games/{gameId}').onUpdate((change, context) => {

  // Retrieve the current and previous value
  const data = change.after.data();
  const previousData = change.before.data();
  const timestamp = admin.firestore.FieldValue.serverTimestamp();
  console.log(previousData);
  console.log(data);

  // We'll only update if the data has changed.
  // This is crucial to prevent infinite loops.
  if (data.posts.length != previousData.posts.length) {
    // Then return a promise of a set operation to update the count
    return change.after.ref.set({
      editedOn: timestamp,
      claimedByUID: null,
      claimedOn: null
    }, {merge: true});
  } else if (data.claimedByUID && !previousData.claimedByUID || data.claimedByUID !== previousData.claimedByUID) {
    console.log('changes');
    console.log(data);
    return change.after.ref.set({
      claimedOn: timestamp
    }, {merge: true});
  } else {
    console.log('no changes');
    return null;
  }
});

// exports.claimGame = functions.https.onCall((data, context) => {
//   return cors(data, context, () => {
//     const game = data.game;
//     const userId = context.auth.uid;
//     return admin.database().ref('/games').doc(game.id);
//       var claimedRef = {uid: userId, time: admin.firestore.FieldValue.serverTimestamp()};
//
//     gameRef.set({
//       claimedBy: claimedRef
//     }, {merge: true});
//     game['claimedBy'] = claimedRef;
//   });
// });

exports.uploadFile = functions.https.onRequest((req, res) => {

  return cors(req, res, () => {
    if (req.method !== 'POST') {
      return res.status(500).json({
        message: 'Not allowed'
      });
    }
    const busboy = new Busboy({headers: req.headers});
    let uploadData = {name: '', type: ''};
    busboy.on('field', (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
      uploadData.name = val;
    });
    busboy.on('file',
      (fieldname, file, filename, encoding, mimetype) => {
        const filepath = path.join(os.tmpdir(), uploadData.name);
        uploadData = {file: filepath, type: mimetype};
        file.pipe(fs.createWriteStream(filepath));
      });

    busboy.on('finish', () => {
      const bucket = gcs.bucket('vue-epyc.appspot.com');
      return bucket.upload(uploadData.file, {
        uploadType: 'media',
        metadata: {
          metadata: {
            name: uploadData.name,
            contentType: uploadData.type
          }
        }
      }).then((data) => {
        const file = data[0];
        file.getSignedUrl({ action: 'read', expires: '03-17-2025'}).then(signedUrl => {
          const url = signedUrl[0];
          res.status(200).json({
            url: url,
          })
        });
      })
        .catch(err => {
          res.status(500).json({
            error: err
          });
        })
    });
    busboy.end(req.rawBody);
  })
});
